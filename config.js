var defaultSettings = {
    "min": 1,
    "max": 30,
    "totalSeconds": 30,
    "reaction": {
        "like": true,
        "love": true,
        "haha": true,
        "wow" : true,
        "sad" : true,
        "angry" : true
    }
};