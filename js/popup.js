$("#run").click(()=>{
    chrome.runtime.sendMessage({action: "initiateInvite"}, ()=>{
        window.close();
    });
});

$("#stop").click(()=>{
    chrome.runtime.sendMessage({action: "stopInvite"}, ()=>{
        window.close();
    });
});


$("#settings").click(()=>{
    chrome.tabs.create({url: chrome.runtime.getURL("settings/index.html")});
});

