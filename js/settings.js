$(document).ready(function(){

init();

$("#saveButton").click(function(){
    var min = $("#fromRange").val();
    var max = $("#toRange").val();
    var like = ($("#like:checked").length>0 ? true : false);
    var love = ($("#love:checked").length>0 ? true : false);
    var haha = ($("#haha:checked").length>0 ? true : false);
    var wow = ($("#wow:checked").length>0 ? true : false);
    var sad = ($("#sad:checked").length>0 ? true : false);
    var angry = ($("#angry:checked").length>0 ? true : false);

    var tempSettings = {
        "min": min,
        "max": max,
        "totalSeconds": 30,
        "reaction": {
            "like": like,
            "love": love,
            "haha": haha,
            "wow" : wow,
            "sad" : sad,
            "angry" : angry
        }
    };

    chrome.storage.local.set({settings: tempSettings}, ()=>{
        alert("SUCCESS");
    });

});


});


function init(){
    setup();
}


function setup(){
    for(var i=1; i<=defaultSettings.totalSeconds; i++) $("#fromRange").append("<option value='"+i+"'>"+i+"</option>");
    adjustToRange(1);
    $("#fromRange").change(() => adjustToRange(Number($(this).val())));


    // Set reaction default
    chrome.storage.local.get("settings", (e)=>{
        var settings = e.settings;
        var reaction = settings.reaction;
        $("#like").attr("checked", reaction.like);
        $("#love").attr("checked", reaction.love);
        $("#haha").attr("checked", reaction.haha);
        $("#wow").attr("checked", reaction.wow);
        $("#sad").attr("checked", reaction.sad);
        $("#angry").attr("checked", reaction.angry);
    });
}


function adjustToRange(fromValue){
    $("#toRange").html("");
    for(var i=fromValue+1 ; i<=defaultSettings.totalSeconds; i++) {
        $("#toRange").append("<option value='"+i+"'>"+i+"</option>");
    }
}
