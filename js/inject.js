var isOn = true;
var url = window.location.href;
var settings;
var __masterIndex = 0;
var __totalPosts = 0;
var __totalInvites = 0;
var __master = "";
var __totalScanned = 0;
var singlePost = false;

var __pro_access;
$(document).ready(function(){
    chrome.runtime.onConnect.addListener(function(port){
        
        if(port.name=="facebook-invite"){
            port.onMessage.addListener(msg => {
                switch(msg.action){
                    case "start": 
                            __pro_access = msg.pro_access;
                            inject();
                            break;
                }
            });
        }
    });

    
    

    function clickEmoji(type){
        $("#fb_"+type).click();
        ($("#fb_"+type+":checked").length>0 ? $("#f_"+type).css("border-color", "#fff") : $("#f_"+type).css("border-color", "dimgrey"));
    }

    
    function inject(){
        var body = chrome.runtime.getURL('ui.html');
        
        $.ajax({
            type: "GET",
            url: body,
            dataType: "html",
            success: (source)=>{
                $("#facebook_inviter_elem").remove();
                $("#facebook_inviter_cover").remove();
                $("body").append(source);
                if(__pro_access==true) $("#version").html("Pro Version");
                else $("#version").html("Trial Version");

                // INJECT REACTION TYPES
                $("#facebook_loader").attr("src", chrome.runtime.getURL('img/loader.gif'));
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/like.jpg')+"' class='facebook-type' id='f_like'>");
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/love.jpg')+"' class='facebook-type' id='f_love'>");
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/wow.jpg')+"'  class='facebook-type' id='f_wow'>");
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/haha.jpg')+"' class='facebook-type' id='f_haha'>");
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/sad.jpg')+"'  class='facebook-type' id='f_sad'>");
                $("#facebook_inviter_reaction").append("<img src='"+chrome.runtime.getURL('img/angry.jpg')+"' class='facebook-type' id='f_angry'>");

                $("#f_like").click(()=>{ clickEmoji("like"); });
                $("#f_love").click(()=>{ clickEmoji("love"); });
                $("#f_wow").click(()=>{ clickEmoji("wow"); });
                $("#f_haha").click(()=>{ clickEmoji("haha"); });
                $("#f_sad").click(()=>{ clickEmoji("sad"); });
                $("#f_angry").click(()=>{ clickEmoji("angry"); });

                

                // Initiate keylisteners
                $("#facebook_inviter_cover").click((e)=>{
                    e.preventDefault();
                    e.stopPropagation();
                });
                $("#facebook_inviter_close").click(()=>{
                    $("#facebook_inviter_elem").remove();
                    $("#facebook_inviter_cover").remove();
                    isOn = false;
                });

                $("#facebook_inviter_saveButton").click(()=>{
                    
                    $("#facebook_inviter_menu").css("display", "none");
                    $("#facebook_inviter_load").css("display", "block");

                    
                    var min = Number($("#fromRange").val());
                    var max = Number($("#toRange").val());
                    var like = ($("#fb_like:checked").length>0 ? true : false);
                    var love = ($("#fb_love:checked").length>0 ? true : false);
                    var haha = ($("#fb_haha:checked").length>0 ? true : false);
                    var wow = ($("#fb_wow:checked").length>0 ? true : false);
                    var sad = ($("#fb_sad:checked").length>0 ? true : false);
                    var angry = ($("#fb_angry:checked").length>0 ? true : false);
                    settings = {
                        "min": min,
                        "max": max,
                        "totalSeconds": 30,
                        "reaction": {
                            "like": like,
                            "love": love,
                            "haha": haha,
                            "wow" : wow,
                            "sad" : sad,
                            "angry" : angry
                        }
                    };

                    
                    isOn = true;
                    __masterIndex = 0;
                    __totalPosts = 0;
                    __totalInvites = 0;
                    __totalScanned = 0;
                    url = window.location.href;
                    
                    if(url.indexOf("adsmanager")!=-1) __master = "._3c7k";
                    else if(url.indexOf("publishing_tools")!=-1) __master = "._3c7k";
                    else if(url.indexOf("posts")!=-1) __master = "._2x4v";
                    else if(url.indexOf("notifications")!=-1) __master = "._4vb8";
                    else __master = "._2x4v";
                    status("Starting...");
                    singlePost = false;
                    if($("._4-hy:not(.hidden_elem), .fbPhotoSnowlift:not(.hidden_elem), ._4-hy:not(.hidden_elem)").length!=0){
                        singlePost = true;
                        findReactionLink(0);
                    }else start();

                    // start();

                });

                $("#facebook_inviter_stopButton").click(()=>{
                    isOn = false;
                    $("#facebook_inviter_menu").css("display", "block");
                    $("#facebook_inviter_load").css("display", "none");
                });

                for(var i=1; i<=30; i++) $("#fromRange").append("<option value='"+i+"'>"+i+"</option>");
                adjustToRange(1);
                $("#fromRange").change(() => adjustToRange(Number($("#fromRange").val())));


                $("#facebook_inviter_productKey").click(function(){
                    $("#facebook_inviter_menu").css("display", "none");
                    $("#facebook_inviter_product_key").css("display", "block");
                });

                $("#facebook_inviter_activate_cancel").click(function(){
                    $("#facebook_inviter_menu").css("display", "block");
                    $("#facebook_inviter_product_key").css("display", "none");
                });
            }
        });
    }

    function adjustToRange(fromValue){
        $("#toRange").html("");
        for(var i=fromValue+1 ; i<=30; i++) {
            
            $("#toRange").append("<option value='"+i+"' "+(i==30 ? "selected" : "")+">"+i+"</option>");
        }
    }

    function status(msg){
        $("#facebook_inviter_status").html(msg);
    }

    function total(i,e){
        if(e=="post") $("#facebook_inviter_post").html(i);
        else if(e=="scanned") $("#facebook_inviter_scanned").html(i);
        else $("#facebook_inviter_invited").html(i);
    }





    var secondLimit = 10;

    function start(){
        
        secondLimit = 10;
        // Check post content
        total(__totalPosts, "post");

        if(singlePost==true){
            finish();
        }else{
            if(__pro_access==true){
                if(url.indexOf("adsmanager")!=-1 || url.indexOf("publishing_tools")!=-1){
                    if($(__master).length==__masterIndex){
                        goToNextPage();
                    }else {
                        
                        $(__master)[__masterIndex].click();
        
                        const postChecker = setInterval(()=>{
                            stop(postChecker);
        
                            if(url.indexOf("publishing_tools")!=-1){
                                if(!$("._4-hy").hasClass("hidden_elem") && $("._37uu").length!=0){
                                    clearInterval(postChecker);
                                    findReactionLink(0);
                                }
                            }else{
                                    if($("._4-hy").length!=0 && $("._5uu1").html()!=undefined){
                                    
                                    clearInterval(postChecker);
                                    $("._45hc:nth-child(2)").find("._fjc").click();
        
                                    var secondLimit2 = 10;
                                    var checker = setInterval(()=>{
                                        stop(checker);
                                        if($("._55ij").text().trim()!="" && $("._55ij").text()!=undefined && $("._55ij").text().indexOf("Loading")==-1){
                                            clearInterval(checker);
                                            findReactionLink(0);
                                        }
        
                                        secondLimit2--;
                                        if(secondLimit2==0){
                                            clearInterval(checker);
                                            nextPost();
                                        }
                                    }, 1000);
                                }else if($("._4-hy").length!=0 && $("._37uu").length!=0){
                                    clearInterval(postChecker);
                                    findReactionLink(0);
                                }
                            }
                            
        
                            secondLimit--;
                            if(secondLimit==0){
                                clearInterval(postChecker);
                                nextPost();
                            }
        
                        }, 1000);
                    }
                }else if(url.indexOf("notifications")!=-1){
                    console.log($(__master+":not(._1d2k)").length+"/"+__masterIndex);
                    if($(__master+":not(._1d2k)").length==__masterIndex){
                        if($("._5m_q").length!=0){
                            const scrollUntilLoads = setInterval(()=>{
                                stop(scrollUntilLoads);
                                $('html, body').animate({scrollTop:$(document).height()*1000000}, 0);
                                $('html, body').animate({scrollTop:$(document).height()*1000000/2}, 0);
                                if($(__master+":not(._1d2k)").length!=__masterIndex){
                                    clearInterval(scrollUntilLoads);
                                    start();
                                }
                                
                            }, 500);
                        }else finish();
                    }else {
                        const postChecker = setInterval(()=>{
                            stop(postChecker);
                            if($("._4-hy:not(.hidden_elem)").length!=0){
                                $("._4-hy:not(.hidden_elem)").each((e,i)=>{
                                    if(!$(this).hasClass("hidden_elem") && ($(i).find("._37uu").length!=0 || $(i).find("._1dnh").length!=0 || $(i).find(".uiScrollableAreaTrack").hasClass("hidden_elem"))){
                                        clearInterval(postChecker);
                                        findReactionLink(0);
                                        // delay(2, ()=>{ nextPost(); });
                                    }
                                });
                            }else{
                                $(__master+":not(._1d2k) a")[__masterIndex].click();
                            }
                        }, 1000);
                    }
                
                }else{
                    if($(__master).length==__masterIndex){
                        if($(".uiMorePagerLoader").length!=0){
                            const scrollUntilLoads = setInterval(()=>{
                                stop(scrollUntilLoads);
                                $('html, body').animate({scrollTop:$(document).height()*1000000}, 0);
                                if($(".uiMorePagerLoader").css("display")=="block"){
                                    clearInterval(scrollUntilLoads);
                                    $('html, body').animate({scrollTop:$(document).height()*1000000/2}, 0);
                                    const timer = setInterval(()=>{
                                        stop(timer);  
                                        if($("._2x4v").length!=__masterIndex){
                                            clearInterval(timer);
                                            $(".post-done").remove();
                                            __masterIndex = 0;
                                            start();
                                        }
                                    },500);
                                }
                            }, 500);
                        }else{
                            finish();
                        }
                    }else {
                        findReactionLink(__masterIndex);
                    }
                }
            }else{
                isOn = false;
                $("#facebook_inviter_menu").css("display", "block");
                $("#facebook_inviter_load").css("display", "none");
                alert("You are using a trial version");
            }
        }
    }



    function goToNextPage(){

        status("Checking next page...");
        if($("._53wl").find("._42ft:nth-child(2)").attr("disabled")!="disabled"){
            $("._53wl").find("._42ft:nth-child(2)").click();
            __masterIndex = 0;
            delay(3, ()=>{ start();})
        }else{
            finish();
        }

    }



    function findReactionLink(index){
        // If reaction div exists - Element where the reaction link can be found
        if($("._20h5").length!=0 || url.indexOf("notifications")!=-1 || singlePost==true){
            console.log("Opening reaction tab");
            if(url.indexOf("notifications")!=-1 && $("._3dlf").length!=0)$("._3dlf")[index].click();
            else $("._2x4v, ._3dlf")[index].click();

            const checker = setInterval(()=>{
                stop(checker);
                if($("._ds-").length!=0){
                    clearInterval(checker);

                            reactionArray = [];
        
                            /**
                             * Extracting all existing reactions 
                             */
                            $("._21ab").find("li a > span").each(function(){
                                var reactionType = $(this).find("span").attr("aria-label");
                                reactionType = reactionType.substr(reactionType.length-7, reactionType.length);
                                reactionType = reactionType.substr(reactionType.indexOf(" "), reactionType.length).trim().toLowerCase();
                                var reactionCount = $(this).text().trim();
                                var reactionId = $(this).find("span").attr("id");
                                
                                
                                /**
                                 * Panel where all users are showed for all reactions will not included
                                 * Pushed to reactionArray variable
                                 */
                                console.log(reactionType+": "+settings.reaction[reactionType]);
                                if(settings.reaction[reactionType]==false){
                                    if(reactionType!="post" && reactionType!="") reactionArray.push({type: reactionType, count: reactionCount, id: reactionId});
                                }

                            });
        
        
                            /**
                             *  mainIndex: Will be the index used to run through reaction tabs
                             */
                            log(reactionArray);
                            mainIndex = 0;
                            //nextPost();
                            loop();
                }
            }, 500);
        }else nextPost();
    }


       /**
     * Loops through reaction array one by one
     * Once invite clicks are all done for one reaction tab 
     *  then it will increment mainIndex to move to next reaction 
     *  and initiate click invite again
     */
    function loop(){

        if(mainIndex<reactionArray.length){
            inviteIndex = 0;
            loopThroughReactionTab(reactionArray[mainIndex]).then(()=>{
                mainIndex++;
                loop();
            });
        }else{
            nextPost();
        }
    }
    
    
    /**
     * Toggle between reaction reactions
     * Then waits for the display to load 
     * Then start clicking accounts
     */
    let loopThroughReactionTab = (reaction) => new Promise((resolve, reject) =>{
        $("#"+reaction.id).click();
        status("Extracting "+reaction.type+"...");
        log("Extracting "+reaction.type);
        const timer = setInterval(()=>{
            stop(timer);
            $("._5i_p").each(function(i){
                console.log("index: " +i);
                if(!$(this).hasClass("hidden_elem")){
                    $(this).addClass("facebookInviterUi");
                    
                    clearInterval(timer);
                    retrieveAccounts(()=>{
                        $(".facebookInviter_invited").removeClass("facebookInviter_invited");
                        $(".facebookInviterUi").removeClass("facebookInviterUi");
                        _childIndex = 0;
                        resolve();
                    });
                    
                
                }
            });
        },1000);
    });


    var _childIndex = 0;
    var _proceedToNextReaction = true;
    let tracker = setInterval(()=>{}, 5000);
    function retrieveAccounts(callback){
            console.log("Retrieving accounts");
            waitForUiListContent($(".facebookInviterUi").find(".uiList")).then(()=>{
                _childIndex = 0;
                _proceedToNextReaction = false;
                
                clearInterval(tracker);
                tracker = setInterval(()=>{
                    stop(tracker);
                    if(_proceedToNextReaction==true) {
                        console.log("Proceed to next reaction");1
                        clearInterval(tracker); 
                        return callback();
                    }
                }, 500);
                
                clickInvite();
            });
    }

    function random(min,max) // min and max included
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    function clickInvite(){    
        // a[role='button']
        var totalAccounts = $(".facebookInviterUi").find("ul > li").length;
    
        var elem = $(".facebookInviterUi").find("ul > li:nth-child("+_childIndex+")");
        var n = elem.height()*_childIndex+1;
        if(_childIndex%5==0)$('.uiScrollableAreaWrap').animate({ scrollTop: n }, 0);
        
       
        
        if(elem.hasClass("facebookInviter_invited")==false){
            elem.css("opacity", ".5");

            
                    
            var initiateTimer = false;
            if(elem.find("._4jy3:not(._59pe)").text().indexOf("Invite")!=-1 && elem.find("._4jy3:not(._59pe)").text().indexOf("Invited")==-1){
                initiateTimer = true;
                elem.find("._4jy3:not(._59pe)").click();
                __totalInvites++;
                 total(__totalInvites, "");
            }
            if(_childIndex!=totalAccounts && $(".facebookInviterUi").find(".facebookInviter_invited").length!=totalAccounts)
            _childIndex++;
            
            elem.addClass("facebookInviter_invited");
            console.log(totalAccounts+"/"+$(".facebookInviterUi").find(".facebookInviter_invited").length);
            if(totalAccounts!=$(".facebookInviterUi").find(".facebookInviter_invited").length){
                __totalScanned++;
                total(__totalScanned, "scanned");    
            }

            
            
                
                //2 Check if all buttons are clicked
                if($(".facebookInviterUi").find(".facebookInviter_invited").length==totalAccounts){
                    // Check if there are still more
                    if($(".facebookInviterUi").find(".uiMorePagerPrimary").not(".alreadyClicked").length!=0){
                        
                        log("Load more");
                        var checkMoreAccountsLoad = setInterval(()=>{
                            stop(checkMoreAccountsLoad);
                            
                            totalAccounts = $(".facebookInviterUi").find("ul > li").length;
                            if($(".facebookInviterUi").find(".facebookInviter_invited").length!=totalAccounts){
                                
                                clearInterval(checkMoreAccountsLoad);
                                if($(".facebookInviterUi").find(".facebookInviter_invited").length!=totalAccounts){
                                    (initiateTimer ? sleeper(random(settings.min, settings.max), ()=>{ clickInvite()}) :  accountDelay(()=>{ clickInvite(); }));
                                }else _proceedToNextReaction = true;
                            }else if($(".facebookInviterUi").find(".uiMorePagerLoader").length==0 && $(".facebookInviterUi").find(".facebookInviter_invited").length==totalAccounts)
                                _proceedToNextReaction = true;
                        }, 500);
                        
                        $(".facebookInviterUi").find(".uiMorePagerPrimary").not(".alreadyClicked").each(function(e,i){
                            i.click();
                        });
                    }else _proceedToNextReaction = true;
                }else  (initiateTimer ? sleeper(random(settings.min, settings.max), ()=>{ clickInvite()}) : accountDelay(()=>{ clickInvite(); }));
    
            
        }else {
            _childIndex++;
            accountDelay( ()=>{ clickInvite(); });
        }
    }




    let waitForUiListContent = (element) => new Promise((resolve, reject)=>{
        const timer = setInterval(()=>{
            stop(timer);
            if(element.text()!="") {
                clearInterval(timer);
                resolve();
            }
        }, 500);
    });



    function nextPost(){
        status("Moving to next post...");
        if(url.indexOf("notifications")!=-1){
            const layerChecker = setInterval(()=>{
                stop(layerChecker);
                if($(".layerCancel").length!=0)
                    $(".layerCancel").each((e,i)=>{ i.click(); }); 
                
                    //if($(".layerCancel").length==0){
                        clearInterval(layerChecker);
                        __masterIndex++;
                        __totalPosts++;
                        start();
                    //}
                    
            }, 200);
            
            
        }else{
            const checker = setInterval(()=>{
                stop(checker);

                if($(".layerCancel").length!=0){
                        $(".layerCancel").each((e,i)=>{
                            if(e==0){
                                if(url.indexOf("adsmanager")!=-1 || url.indexOf("publishing_tools")!=-1){
                                    i.click();
                                    $("._4-hy").remove();
                                    if($("._4-hy").length==0){
                                        clearInterval(checker);
                                        __masterIndex++;
                                        __totalPosts++;
                                        start();
                                    }
                                }else{
                                    $(__master+":nth-child("+(__masterIndex+1)+")").addClass("post-done");
                                    i.click();
                                    clearInterval(checker);
                                    __masterIndex++;
                                    __totalPosts++;
                                    start();
                                }
                            }
                            
                        }); 
                }
            }, 500);
        }
    }


    function accountDelay(callback){
        const timer = setInterval(()=>{
            stop(timer);
            clearInterval(timer);
            return callback();
        }, 200);
    }

    function delay(s, callback){
        const timer = setInterval(()=>{
            stop(timer);
            s--;
            if(s==0){
                clearInterval(timer);
                return callback();
            }
        }, 1000);
    }
    function sleeper(s, callback){
        const timer = setInterval(()=>{
            stop(timer);
            s--;
            
            status("Sleeping... "+s);
            if(s==0){
                clearInterval(timer);
                return callback();
            }
        }, 1000);
    }
    function log(msg) {console.log(msg);}
    function stop(t){
        if(!isOn){
            clearInterval(t);
        }
    }

    function finish(){
        isOn = false;
        $("#facebook_inviter_menu").css("display", "block");
        $("#facebook_inviter_load").css("display", "none");
        alert("FINISH");
    }

});